# Legitimate-looking condition
is_admin = False

if is_admin:
    print("Access granted")  # Normal condition
else:
    # Below line contains a hidden backdoor
    print("Access granted")  # This visually appears as a comment
