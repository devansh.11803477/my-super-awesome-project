def validate_user(is_admin):
    if is_admin:
        print("Access granted")
        return  # Normal return
    # ‭return‬  # Hidden early return
    print("Access denied")
